import pybtex.database
from pybtex.database import BibliographyData, Entry

BIB_FILE = "bibfile.bib"

REPLACE_CHRS = "—{}[]():/|\_-,. "

generated_tags = []


def generateLabel(bib_data, entry):
    def replace_all(text):
        for c in REPLACE_CHRS:
            text = text.replace(c, "")
        return text

    # print(bib_data.entries[entry].persons["author"][0].last_names)
    # print(bib_data.entries[entry].persons["author"][0].last_names[0])

    def authorTitle():
        div = ""
        if len(bib_data.entries[entry].persons["author"]) == 0:
            return ""

        out = replace_all(
            bib_data.entries[entry].persons["author"][0].last_names[0])
        if "year" in bib_data.entries[entry].fields:
            out += div + bib_data.entries[entry].fields["year"]
        final_title = out.lower()

        # check how many already present
        n = 0
        for tag in generated_tags:
            if final_title == tag.split("_")[0]:
                n += 1
        if n > 0:
            final_title = "{0}_{1}".format(final_title, n + 1)

        return final_title

    def fallbackTitle():
        out = bib_data.entries[entry].type.title()
        div = ":"

        if "title" in bib_data.entries[entry].fields.keys():
            out += div + \
                replace_all(bib_data.entries[entry].fields["title"].title())

        if "year" in bib_data.entries[entry].fields:
            out += div + bib_data.entries[entry].fields["year"]

        return out

    out = authorTitle()
    if out != "":
        return out
    out = fallbackTitle()
    return out


print("=== Pretty BibTex entry keys ===")
print("> Started\n")

bib_data = pybtex.database.parse_file(BIB_FILE, bib_format='bibtex')
new_bib_data = BibliographyData()

for entry in bib_data.entries:
    # print(bib_data.entries[entry].fields["year"])
    new_label = generateLabel(bib_data, entry)
    generated_tags.append(new_label)

    print(new_label)
    entryO = bib_data.entries[entry]
    new_bib_data.add_entry(new_label, entryO)

# print(new_bib_data.to_string('bibtex'))
new_bib_data.to_file(BIB_FILE, bib_format='bibtex')

print("\n> Done! Saved %d entries" % len(new_bib_data.entries))
